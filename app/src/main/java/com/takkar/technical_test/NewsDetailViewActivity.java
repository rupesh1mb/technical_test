package com.takkar.technical_test;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class NewsDetailViewActivity extends AppCompatActivity {

    public static final String EXTRA_LINK_URL = "LINK_URL";
    public static final String TAG = "NEWS_DETAIL_VIEW";

    WebView webview;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail_view);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setLogo(R.drawable.logo_b2c_small);
        getSupportActionBar().setTitle(getResources().getString(R.string.empty));

        String url = getIntent().getStringExtra(EXTRA_LINK_URL);

        webview = (WebView) findViewById(R.id.webView);

        WebSettings webViewSettings = webview.getSettings();

        webViewSettings.setJavaScriptEnabled(true);

        webview.setWebViewClient(new WebViewClient());
        Log.d(TAG, "Loading the url: " + url);
        webview.loadUrl(url);
    }

    private ShareActionProvider mShareActionProvider;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.news_detail_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_share:
                Log.d(TAG, "Share selected");
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "View Article");
                shareIntent.putExtra(Intent.EXTRA_TEXT, getIntent().getStringExtra(EXTRA_LINK_URL));
                shareIntent.setType("text/plain");
                startActivity(Intent.createChooser(shareIntent, "Share via"));
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

package com.takkar.technical_test.newsfeed;

import com.takkar.technical_test.NewsFeedItem;

import java.util.List;

/**
 * Created by rupesh1mb on 2/1/2017.
 */

public interface INewsFeed {
    String getNewsFeedStringFromNetwork();
    List<NewsFeedItem> parseNewsFeedString(String response);
}

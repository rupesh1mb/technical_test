package com.takkar.technical_test.newsfeed;

import android.util.Log;

import com.takkar.technical_test.NewsFeedItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by rupesh1mb on 2/1/2017.
 */

public class ABCNewsFeed implements INewsFeed {

    public static final String URL_PATH = "https://api.rss2json.com/v1/api.json?rss_url=http://www.abc.net.au/news/feed/51120/rss.xml";

    private static final SimpleDateFormat jsonResponseDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final String TAG = "ABCNewsFeed";

    @Override
    public String getNewsFeedStringFromNetwork() {
        try {
            return NetworkUtil.fetchStringFromURL(URL_PATH);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    @Override
    public List<NewsFeedItem> parseNewsFeedString(String responseString) {

        ArrayList<NewsFeedItem> result = new ArrayList<NewsFeedItem>();

        try {
            JSONObject response = new JSONObject(responseString);

            JSONArray items = response.getJSONArray("items");

            for (int i = 0; i < items.length(); i++) {
                try {
                    result.add(parseNewsFeedItemFromJSON(items.getJSONObject(i)));
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "Found " + result.size() + " news items entry.");

        return result;
    }

    private NewsFeedItem parseNewsFeedItemFromJSON(JSONObject jsonItem) throws JSONException {

        Calendar pubDate = Calendar.getInstance();

        try {
            pubDate.setTime(jsonResponseDateFormat.parse(jsonItem.getString("pubDate")));
        } catch (ParseException e) {
            pubDate = null;
        }

        return new NewsFeedItem(
                jsonItem.getString("title"),
                jsonItem.getString("description"),
                pubDate,
                jsonItem.getString("thumbnail"),
                jsonItem.getString("link"));
    }
}

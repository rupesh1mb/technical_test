package com.takkar.technical_test.newsfeed;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by rupesh1mb on 2/1/2017.
 */

public class NetworkUtil {

    public static final String TAG = "NetworkUtil";

    public static Bitmap fetchImageFromURL(String imageurl) {
        try {
            InputStream in = new URL(imageurl).openStream();
            return BitmapFactory.decodeStream(in);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String fetchStringFromURL(String urlpath) throws IOException {
        URL url;
        try {
            url = new URL(urlpath);
        } catch (MalformedURLException e) {
            e.printStackTrace();

            return null;
        }

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();

        int responseCode = connection.getResponseCode();
        Log.d(TAG, "Network request response code: " + responseCode);
        //need to validate the response was successful!

        BufferedReader responseReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

        String data = "";
        StringBuilder responseBuilder = new StringBuilder();
        while ((data = responseReader.readLine()) != null) {
            responseBuilder.append(data);
        }

        return responseBuilder.toString();
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = cm.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }
}

package com.takkar.technical_test.newsfeed;

import com.takkar.technical_test.NewsFeedItem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by rupesh1mb on 2/1/2017.
 */

public class MockNewsFeed implements INewsFeed {
    @Override
    public String getNewsFeedStringFromNetwork() {
        return "";
    }

    @Override
    public List<NewsFeedItem> parseNewsFeedString(String response) {
        ArrayList<NewsFeedItem> result = new ArrayList<NewsFeedItem>();

        result.add(new NewsFeedItem("This is title1. Lets put some long long and very long title in place to see the effect. This is title1. Lets put some long long and very long title in place to see the effect. This is title1. Lets put some long long and very long title in place to see the effect. This is title1. Lets put some long long and very long title in place to see the effect.", "This is description1", Calendar.getInstance(), null, "Link1"));
        result.add(new NewsFeedItem("This is title2", "This is description2", Calendar.getInstance(), null, "Link2"));
        result.add(new NewsFeedItem("This is title3", "This is description3", Calendar.getInstance(), null, "Link3"));
        result.add(new NewsFeedItem("This is title4", "This is description4", Calendar.getInstance(), null, "Link4"));

        return result;
    }
}

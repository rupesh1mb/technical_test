package com.takkar.technical_test.adapters;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.takkar.technical_test.ImageCacherAndDownloader;
import com.takkar.technical_test.NewsFeedItem;
import com.takkar.technical_test.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rupesh1mb on 1/31/2017.
 */

public class NewsFeedListAdapter extends RecyclerView.Adapter<NewsFeedListAdapter.ViewHolder> {

    private List<NewsFeedItem> mNewsFeedItems;
    public static final SimpleDateFormat displayDateFormat = new SimpleDateFormat("MMMM d, yyyy hh:mm a");
    private NewsFeedItemClickListener mItemClickListener;
    private ImageCacherAndDownloader mImageCacherAndDownloader;

    public NewsFeedItem getItem(int position) {
        return mNewsFeedItems.get(position);
    }

    public interface NewsFeedItemClickListener {
        void onNewsFeedItemClick(NewsFeedItem newsFeedItem);
    }

    public NewsFeedListAdapter(List<NewsFeedItem> newsFeedItems, NewsFeedItemClickListener listener, ImageCacherAndDownloader imageCacherAndDownloader) {
        mNewsFeedItems = (newsFeedItems == null) ? new ArrayList<NewsFeedItem>() : newsFeedItems;
        mItemClickListener = listener;
        mImageCacherAndDownloader = imageCacherAndDownloader;
    }

    public void setNewsFeedItems(List<NewsFeedItem> newsFeedItems) {
        if(newsFeedItems == null)
            this.mNewsFeedItems.clear();
        else
            this.mNewsFeedItems = newsFeedItems;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_article_card_view, parent, false);

        final ViewHolder viewHolder = new ViewHolder(v);

        if(mItemClickListener != null) {
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mItemClickListener != null) {
                        NewsFeedItem clickedItem = mNewsFeedItems.get(viewHolder.getAdapterPosition());
                        Log.d("NEWS", "Clicked news feed item: " + clickedItem.toString());
                        mItemClickListener.onNewsFeedItemClick(clickedItem);
                    }
                }
            });
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setNewsItem(mNewsFeedItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mNewsFeedItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mtxtTitle, mtxtPubDate;
        public ImageView mImgThumbnail;

        public ViewHolder(View itemView) {
            super(itemView);
            mtxtTitle = (TextView) itemView.findViewById(R.id.txtNewsTitle);
            mtxtPubDate = (TextView) itemView.findViewById(R.id.txtPubDate);
            mImgThumbnail = (ImageView) itemView.findViewById(R.id.imgViewThumbnail);
        }

        public  void setNewsItem(NewsFeedItem item) {
            mtxtTitle.setText(item.getTitle());
            mtxtPubDate.setText(displayDateFormat.format(item.getPublishedDate().getTime()));
            Bitmap thumbnail = mImageCacherAndDownloader.getImage(item.getThumbnailUrl());
            mImgThumbnail.setImageBitmap(thumbnail);
            mImgThumbnail.setVisibility(thumbnail == null ? View.GONE : View.VISIBLE);
        }
    }

    public void updateImage(String url, Bitmap image) {

    }
}

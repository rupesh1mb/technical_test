package com.takkar.technical_test;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.takkar.technical_test.cache.CachedImageEntry;
import com.takkar.technical_test.cache.ImageBitmapCache;
import com.takkar.technical_test.newsfeed.NetworkUtil;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by rupesh1mb on 2/1/2017.
 * Maintains a cache for images.
 * All the request for images are handled through this class.
 * Whenever a request appears for an image, it tries to return it from the cache.
 * If the
 */

public class ImageCacherAndDownloader {
    public static final int MAX_CACHE_SIZE = 20;
    private static final String TAG = "ImgCacheHandler";
    private int PLACE_HOLDER_IMAGE_RESOURCE_ID = R.drawable.no_image_avaliable;

    ExecutorService executorService;

    ImageDownloader.OnImageAvailableListener mOnImageAvailableListener;

    ImageBitmapCache mImageBitmapCache;

    Bitmap placeHolderImage;

    Context mContext;

    //context is required here because in cache we have used sharedpreference which requires the context object
    public ImageCacherAndDownloader(Context context, ImageDownloader.OnImageAvailableListener listener) {
        mContext = context;
        executorService = Executors.newSingleThreadExecutor();
        mOnImageAvailableListener = listener;
        mImageBitmapCache = ImageBitmapCache.getInstance(context, MAX_CACHE_SIZE);
        placeHolderImage = BitmapFactory.decodeResource(context.getResources(), PLACE_HOLDER_IMAGE_RESOURCE_ID);
    }

    /*
    If the cache hit then return actual image otherwise return placeholder image
    When cache misses, submit a task to execution thread for downloading the image
    When the image download completes, it is notified through the listener, here we update the
    cache and then notify the client about the bitmap being available
     */
    public Bitmap getImage(String url) {

        //this implies that there is no image to display
        //in the UI hide the imageview
        if(url == null || url.trim().length() == 0)
            return null;

        CachedImageEntry entry = mImageBitmapCache.getCacheEntry(url);
        if(entry != null) {
            return entry.getBitmapImage();
        }
        else if(NetworkUtil.isNetworkAvailable(mContext)) //if online then download the image otherwise no need to download
        {
            Log.d("", "Cache missed for url: " + url + ", cache count: ");
            executorService.submit(new ImageDownloader(url, new ImageDownloader.OnImageAvailableListener() {
                @Override
                public void OnImageAvailable(String url, Bitmap image) {
                    mImageBitmapCache.addToCache(url, image);

                    //notify the client that image is available
                    if(mOnImageAvailableListener != null)
                        mOnImageAvailableListener.OnImageAvailable(url, image);
                }
            }));
        }

        return placeHolderImage;
    }

    public void stop() {
        executorService.shutdown();
        try {
            executorService.awaitTermination(3000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

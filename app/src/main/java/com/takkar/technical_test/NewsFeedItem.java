package com.takkar.technical_test;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by rupesh1mb on 1/31/2017.
 */

public class NewsFeedItem implements Parcelable {

    private static final SimpleDateFormat jsonResponseDateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");

    private final String mTitle;

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public Calendar getPublishedDate() {
        return mPublishedDate;
    }

    public String getThumbnailUrl() {
        return mThumbnailUrl;
    }

    public String getArticleURL() {
        return mArticleURL;
    }

    private final String mDescription;
    private final Calendar mPublishedDate;
    private final String mThumbnailUrl;
    private final String mArticleURL;

    SimpleDateFormat df = new SimpleDateFormat("dd/mm/yyyy");

    public NewsFeedItem(String title, String description, Calendar publishdedDate, String thumbNail, String articleURL) {
        mTitle = title;
        mDescription = description;
        mPublishedDate = publishdedDate;
        mThumbnailUrl = thumbNail;
        mArticleURL = articleURL;
    }

    private NewsFeedItem(Parcel p) {
        this.mTitle = p.readString();
        this.mDescription = p.readString();

        long timeInMillis = p.readLong();
        Calendar publishedDate = Calendar.getInstance();
        if (timeInMillis == -1) {
            publishedDate = null;
        } else {
            publishedDate.setTimeInMillis(timeInMillis);
        }
        this.mPublishedDate = publishedDate;

        this.mThumbnailUrl = p.readString();
        this.mArticleURL = p.readString();
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeString(mDescription);
        dest.writeLong(mPublishedDate == null ? -1 : mPublishedDate.getTimeInMillis());
        dest.writeString(mThumbnailUrl);
        dest.writeString(mArticleURL);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<NewsFeedItem> CREATOR = new Parcelable.Creator<NewsFeedItem>() {

        @Override
        public NewsFeedItem createFromParcel(Parcel source) {
            return new NewsFeedItem(source);
        }

        @Override
        public NewsFeedItem[] newArray(int size) {
            return new NewsFeedItem[size];
        }
    };

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("Title: ").append(mTitle).append("\n");
        builder.append("Description: ").append(mDescription).append("\n");

        builder.append("Pub Date: ").append(mPublishedDate == null ? "N/A" : df.format(mPublishedDate.getTime())).append("\n");

        builder.append("ThumbURL: ").append(mThumbnailUrl).append("\n");
        builder.append("ArticleURL: ").append(mArticleURL).append("\n");

        return builder.toString();
    }
}

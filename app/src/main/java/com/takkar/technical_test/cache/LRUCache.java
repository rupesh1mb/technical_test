package com.takkar.technical_test.cache;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by rupesh1mb on 2/1/2017.
 * An implementation of a LRU cache using LinkedHashMap
 * When instantiating it's instance, need to provide the max capacity
 * when the cache is full, the least accessed item is replaced by the next item available for insertion.
 */

public class LRUCache<K, V> {

    private class BoundedLinkedHashMap<K, V> extends LinkedHashMap<K, V> {

        public final int MAX_CAPACITY;

        public BoundedLinkedHashMap(int capacity) {
            MAX_CAPACITY = capacity;
        }

        @Override
        protected boolean removeEldestEntry(Entry<K, V> eldest) {
            return this.size() > this.MAX_CAPACITY;
        }
    }

    BoundedLinkedHashMap<K, V> mCache;

    public LRUCache(int maxCapacity) {
        mCache = new BoundedLinkedHashMap<K, V>(maxCapacity);
    }

    public V getCachedValueFor(K key) {
        if(mCache.containsKey(key)) {
            V result = mCache.get(key);
            insertIntoCache(key, result);
            return result;
        }

        return null;
    }

    public void insertIntoCache(K key, V value) {
        if(mCache.containsKey(key)) {
            mCache.remove(key);
        }

        mCache.put(key, value);
    }

    public int getSize() {
        return mCache.size();
    }

    public void clear() {
        mCache.clear();
    }
}

package com.takkar.technical_test.cache;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Map;

/**
 * Created by rupesh1mb on 2/1/2017.
 * A class that provides in memory and disk cache.
 * In memory, an image is keps as bitmap object
 * In disk, the images are stored in app's cache location
 * And the mapping of url to the cached images in disk is maintained through SharedPreferences.
 */

public class ImageBitmapCache {

    private static final String PREFERENCE_NAME = "Images_Cache";
    public static final String TAG = "ImageBitmapCache";

    SharedPreferences mUrlToDiskImageMapper;

    File CacheLocation;

    private static ImageBitmapCache mInstance;

    //key is url and value is memory bitmap, disk bitmap location
    LRUCache<String, CachedImageEntry> mLRUCache;

    private ImageBitmapCache(Context context, int maxCapacity) {
        mLRUCache = new LRUCache<String, CachedImageEntry>(maxCapacity);
        CacheLocation = new File(context.getCacheDir(), "tmp_image");
        mUrlToDiskImageMapper = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    /*
    The cache is a singleton instance...
     */
    public static ImageBitmapCache getInstance(Context context, int maxCapicity) {
        if (mInstance == null) {
            mInstance = new ImageBitmapCache(context, maxCapicity);

            mInstance.rebuildCacheFromSharedPreference();
        }

        return mInstance;
    }

    /*
    For the first time when the app starts, it tries to rebuild the cache from
    the last time cached images from disk
     */
    private void rebuildCacheFromSharedPreference() {
        Map<String, ?> spEntries = mUrlToDiskImageMapper.getAll();

        if (spEntries != null) {
            for (Map.Entry<String, ?> entry : spEntries.entrySet()) {
                Bitmap image = readImageFromDisk(entry.getValue().toString());

                if (image != null) {
                    CachedImageEntry cacheEntry = new CachedImageEntry();
                    cacheEntry.setUrl(entry.getKey());
                    cacheEntry.setImagePathInDisk(entry.getValue().toString());
                    cacheEntry.setBitmapImage(image);

                    mLRUCache.insertIntoCache(entry.getKey(), cacheEntry);
                }
            }
        }
    }

    public CachedImageEntry getCacheEntry(String url) {
        if(url == null)
            return null;

        url = url.trim().toLowerCase();
        return mLRUCache.getCachedValueFor(url);
    }

    public CachedImageEntry addToCache(String url, Bitmap image) {
        CachedImageEntry cacheImageEntry = null;

        //first write the image to disk
        String diskLocation = writeImageToDisk(image);

        //then update the url to path mapper in shared preference
        //If writing the image to disk failed then don't cache that in memory as well
        if (diskLocation != null) {
            SharedPreferences.Editor editor = mUrlToDiskImageMapper.edit();
            editor.putString(url, diskLocation);
            editor.commit();

            //create an instance of CachedImageEntry
            cacheImageEntry = new CachedImageEntry();
            cacheImageEntry.setUrl(url);
            cacheImageEntry.setImagePathInDisk(diskLocation);
            cacheImageEntry.setBitmapImage(image);

            //make an entry to the LRU cache
            mLRUCache.insertIntoCache(url, cacheImageEntry);
        }

        return cacheImageEntry;
    }

    public void clearCache() {
        Log.d("", "The cache is cleared!");
        //clear lru cache
        mLRUCache.clear();
        //clear shared preference
        mUrlToDiskImageMapper.edit().clear().commit();
        //delete all the files from the disk cache
        if(CacheLocation.exists() && CacheLocation.isDirectory()) {
            for(File f : CacheLocation.listFiles()) {
                if(!f.isDirectory())
                    f.delete();
            }
        }
    }

    private Bitmap readImageFromDisk(String filepath) {
        File imageFile = new File(filepath);
        Bitmap result = null;
        if (imageFile.exists()) {
            result = BitmapFactory.decodeFile(filepath);
        }

        return result;
    }

    private String writeImageToDisk(Bitmap bitmap) {
        FileOutputStream out = null;

        if(!CacheLocation.exists())
            CacheLocation.mkdir();

        File outputfile = new File(CacheLocation, String.valueOf(Calendar.getInstance().getTimeInMillis()) + ".png");

        try {
            out = new FileOutputStream(outputfile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            return outputfile.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Image Writing to disk failed: ", e);
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}

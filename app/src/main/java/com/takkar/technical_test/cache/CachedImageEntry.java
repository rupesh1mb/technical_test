package com.takkar.technical_test.cache;

import android.graphics.Bitmap;

/**
 * Created by rupesh1mb on 2/1/2017.
 * This is an entry for a cached item
 * There is memory cache and a disk cache for an image
 * so, there will be 3 property for a cached image.
 * 1. Image URL
 * 2. Disk cached location
 * 3. Bitmap Image in memory
 */

public class CachedImageEntry {
    private String mUrl;
    private String mImagePathInDisk;
    private Bitmap mBitmapImage;

    public Bitmap getBitmapImage() {
        return mBitmapImage;
    }

    public void setBitmapImage(Bitmap mBitmapImage) {
        this.mBitmapImage = mBitmapImage;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public String getImagePathInDisk() {
        return mImagePathInDisk;
    }

    public void setImagePathInDisk(String mImagePathInDisk) {
        this.mImagePathInDisk = mImagePathInDisk;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("URL: ").append(mUrl).append(",\n");
        builder.append("DiskLocation: ").append(mImagePathInDisk).append("\n");
        builder.append("Bitmap is null: ").append(mBitmapImage == null);

        return builder.toString();
    }
}

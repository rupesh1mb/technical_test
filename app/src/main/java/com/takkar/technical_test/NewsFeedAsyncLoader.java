package com.takkar.technical_test;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.takkar.technical_test.cache.ImageBitmapCache;
import com.takkar.technical_test.newsfeed.INewsFeed;
import com.takkar.technical_test.newsfeed.NetworkUtil;

import java.util.List;

/**
 * Created by rupesh1mb on 1/31/2017.
 * This is the class responsible for downloading the json response from the provided URL
 * It provides caching in sharedpreference of the response received for the download news feed get request
 * If there is no network connectivity then, this cached response is used to load the news list (when offline)
 */

public class NewsFeedAsyncLoader extends AsyncTaskLoader<List<NewsFeedItem>> {

    private final String cacheName = "url_response_cache";

    private static final String TAG = "NewsFeedAsyncLoader";

    private INewsFeed mNewsFeedSource;

    SharedPreferences mCache;

    boolean mIsReload;

    public NewsFeedAsyncLoader(Context context, INewsFeed newsFeedSource, boolean isReload) {
        super(context);
        this.mNewsFeedSource = newsFeedSource;
        mCache = context.getSharedPreferences(cacheName, Context.MODE_PRIVATE);
        mIsReload = isReload;
    }

    @Override
    public List<NewsFeedItem> loadInBackground() {
        //clear the cache if the page is being refreshed...
        if(mIsReload) {
            ImageBitmapCache.getInstance(getContext(), 20).clearCache();
        }
        String response = "";

        //online
        if(NetworkUtil.isNetworkAvailable(getContext())) {
            response = mNewsFeedSource.getNewsFeedStringFromNetwork();
            SharedPreferences.Editor cacheEditor = mCache.edit();
            cacheEditor.putString(mNewsFeedSource.getClass().getName(), response);
            cacheEditor.commit();
        } else { //else if offline, read the cached response
            response = mCache.getString(mNewsFeedSource.getClass().getName(), "");
            Log.d(TAG, "Network state offline so showing the result from cache...");
        }

        return mNewsFeedSource.parseNewsFeedString(response);
    }

    @Override
    public void deliverResult(List<NewsFeedItem> data) {
        if (isStarted())
            super.deliverResult(data);
    }
}

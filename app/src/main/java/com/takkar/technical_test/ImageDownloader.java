package com.takkar.technical_test;

import android.graphics.Bitmap;

import com.takkar.technical_test.newsfeed.NetworkUtil;

/**
 * Created by rupesh1mb on 2/1/2017.
 * Runnable Class that encapsulates the logic to downloads an image
 * from the provided url and notifies when download complete.
 * An instance of this class can be fed into executor service for downloading
 */
public class ImageDownloader implements Runnable {

    private OnImageAvailableListener mImageAvailableListener;

    String imageURL;

    public interface OnImageAvailableListener {
        void OnImageAvailable(String url, Bitmap image);
    }

    public ImageDownloader(String url, OnImageAvailableListener imageAvailableListener) {
        imageURL = url;
        mImageAvailableListener = imageAvailableListener;
    }

    @Override
    public void run() {
        Bitmap image = NetworkUtil.fetchImageFromURL(imageURL);

        if (image != null && mImageAvailableListener != null) {
            mImageAvailableListener.OnImageAvailable(imageURL, image);
        }
    }
}

package com.takkar.technical_test;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.takkar.technical_test.adapters.NewsFeedListAdapter;
import com.takkar.technical_test.newsfeed.ABCNewsFeed;
import com.takkar.technical_test.newsfeed.MockNewsFeed;

import java.util.List;

public class ArticleFeedListActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<NewsFeedItem>>{

    private static final int ABC_NEWS_FEED_LOADER = 1;
    private static final int LOCAL_MOCK_NEWS_FEED_LOADER = 0;
    public static final String TAG = "NEWS_FEED_LIST_ACTIVITY";
    public static final String IS_PAGE_RELOAD = "page_reload";

    private RecyclerView mNewsFeedListView;
    private NewsFeedListAdapter mNewsFeedListAdapter;
    private ImageCacherAndDownloader mImageCacherAndDownloader;

    private Toolbar toolbar;
    private TextView txtViewStatus;
    private ProgressBar loading_progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_feed_list);

        txtViewStatus = (TextView) findViewById(R.id.txtStatus);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setLogo(R.drawable.logo_b2c_small);
        getSupportActionBar().setTitle(getResources().getString(R.string.empty));

        mImageCacherAndDownloader = new ImageCacherAndDownloader(this, onImageAvailableListener);

        mNewsFeedListView = (RecyclerView) findViewById(R.id.rclvwNewsFeedList);
        mNewsFeedListView.setHasFixedSize(true);
        mNewsFeedListView.setLayoutManager(new LinearLayoutManager(this));

        mNewsFeedListAdapter = new NewsFeedListAdapter(null, newsFeedItemClickListener, mImageCacherAndDownloader);

        mNewsFeedListView.setAdapter(mNewsFeedListAdapter);

        loading_progress = (ProgressBar) findViewById(R.id.loading_progress);

        getSupportLoaderManager().initLoader(ABC_NEWS_FEED_LOADER, null, this).forceLoad();
        //getSupportLoaderManager().initLoader(LOCAL_MOCK_NEWS_FEED_LOADER, null, this).forceLoad();

        loading_progress.setVisibility(View.VISIBLE);
    }

    ImageDownloader.OnImageAvailableListener onImageAvailableListener = new ImageDownloader.OnImageAvailableListener() {

        //find the item in recyclerview that needs it's thumbnail to be updated
        //make a call to update the image
        @Override
        public void OnImageAvailable(final String url, final Bitmap image) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    LinearLayoutManager layoutManager = (LinearLayoutManager)mNewsFeedListView.getLayoutManager();
                    int start = Math.max(layoutManager.findFirstVisibleItemPosition() - 1, 0);
                    int last = Math.max(layoutManager.findLastVisibleItemPosition() + 1, mNewsFeedListAdapter.getItemCount() - 1);

                    for(int i=start; i <= last; i++) {
                        if(mNewsFeedListAdapter.getItem(i).getThumbnailUrl().trim().toLowerCase().equals(url)) {
                            mNewsFeedListAdapter.notifyItemChanged(i);
                            break;
                        }
                    }
                }
            });
        }
    };

    NewsFeedListAdapter.NewsFeedItemClickListener newsFeedItemClickListener = new NewsFeedListAdapter.NewsFeedItemClickListener() {
        @Override
        public void onNewsFeedItemClick(NewsFeedItem newsFeedItem) {
            showDetailView(newsFeedItem);
        }
    };


    @Override
    public Loader<List<NewsFeedItem>> onCreateLoader(int id, Bundle args) {
        NewsFeedAsyncLoader newsFeedAsyncLoader = null;

        switch (id) {
            case ABC_NEWS_FEED_LOADER:
                boolean isReload = false;
                if(args != null)
                    isReload = args.getBoolean(IS_PAGE_RELOAD, false);
                newsFeedAsyncLoader = new NewsFeedAsyncLoader(this, new ABCNewsFeed(), isReload);
                break;
            case LOCAL_MOCK_NEWS_FEED_LOADER:
                newsFeedAsyncLoader = new NewsFeedAsyncLoader(this, new MockNewsFeed(), false);
                break;
        }

        return newsFeedAsyncLoader;
    }

    @Override
    public void onLoadFinished(Loader<List<NewsFeedItem>> loader, List<NewsFeedItem> data) {
        //if there is no data to display then show the status label
        txtViewStatus.setVisibility(data == null || data.size() == 0 ? View.VISIBLE : View.GONE);
        loading_progress.setVisibility(View.GONE);
        mNewsFeedListAdapter.setNewsFeedItems(data);
    }

    @Override
    public void onLoaderReset(Loader<List<NewsFeedItem>> loader) {

    }

    private void showDetailView(NewsFeedItem newsFeedItem) {
        Intent intent = new Intent(this, NewsDetailViewActivity.class);
        intent.putExtra(NewsDetailViewActivity.EXTRA_LINK_URL, newsFeedItem.getArticleURL());
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.news_feed_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_refresh:
                reloadNewsFeed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void reloadNewsFeed() {
        mNewsFeedListAdapter.setNewsFeedItems(null);
        Bundle args = new Bundle();
        args.putBoolean(IS_PAGE_RELOAD, true);
        getSupportLoaderManager().restartLoader(ABC_NEWS_FEED_LOADER, args, ArticleFeedListActivity.this).forceLoad();
        loading_progress.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        //to stop the background thread used for downloading image
        mImageCacherAndDownloader.stop();

        super.onDestroy();
    }
}
